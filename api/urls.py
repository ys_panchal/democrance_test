from django.urls import path

from api.views import (
    CustomerCreateView,
    QuoteView,
    PolicyView
)

urlpatterns = [
    path('create_customer/', CustomerCreateView.as_view()),
    path('quote/', QuoteView.as_view()),
    path('quote/<int:pk>', QuoteView.as_view()),
    path('policies/', PolicyView.as_view()),
]

