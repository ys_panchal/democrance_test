from api.models import Customer
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination

from api.models import CustomerPolicy
from api.serializers import CustomerSerializer, QuoteSerializer


class CustomerCreateView(generics.CreateAPIView):
    """
    Customer create view
    """
    querySet = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (permissions.AllowAny, )


class QuoteView(APIView):
    """
    Create quote view
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = QuoteSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        customer_policy = get_object_or_404(CustomerPolicy.objects.all(), pk=pk)

        serializer = self.serializer_class(instance=customer_policy, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class policyPagination(PageNumberPagination):
    page_size = 10


class PolicyView(generics.ListAPIView):
    """
    Policy list view
    """
    serializer_class = QuoteSerializer
    pagination_class = policyPagination
    paginate_by = 10

    def get_queryset(self):
        """
        """
        queryset = CustomerPolicy.objects.all()
        customer_id = self.request.query_params.get('customer_id')
        if customer_id:
            return queryset.filter(customer__id=customer_id)
        return queryset
