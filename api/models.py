import secrets
import string
from django.db import models
from django.contrib.auth.models import User


class Customer(User):
    """
    Model to store details of Customer
    """
    dob = models.DateField()

    class Meta:
        verbose_name = 'customer'
        verbose_name_plural = 'customers'

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_username(self):
        """
        Returns the valid username for given user
        """
        random_str = ''.join(
            secrets.choice(
                string.ascii_lowercase + string.digits) for i in range(6))
        username = "{}_{}".format(
            self.first_name.lower(), random_str)
        return username

    def save(self, *args, **kwargs):
        """
        Override the save method to check username for given user
        """
        if not self.username:
            self.username = self.get_username()
        super(Customer, self).save(*args, **kwargs)


class Policy(models.Model):
    """
    Model to store details of policy
    """
    # Policy type choices
    HEALTH_INSURANCE = 'health-insurance'
    TERM_PLAN = 'term-plan'
    LIFE_INSURANCE = 'life-insurance'
    PERSONAL_ACCIDENT = 'personal-accident'
    POLICY_TYPE_LIST = [HEALTH_INSURANCE, TERM_PLAN, LIFE_INSURANCE, PERSONAL_ACCIDENT]
    POLICY_TYPE_CHOICES = ((policy_type, policy_type) for policy_type in POLICY_TYPE_LIST)

    type = models.CharField(max_length=100, choices=POLICY_TYPE_CHOICES)
    premium = models.IntegerField()
    cover = models.IntegerField()
    age_limit = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'policy'
        verbose_name_plural = 'policies'

    def __str__(self):
        return "Type: {} - Preminum: {} - Cover: {} - Age: {}".format(
            self.type, self.premium, self.cover, self.age_limit)


class CustomerPolicy(models.Model):
    """
    Model to store customer policy details
    """
    # Customer policy choices
    QUOTED = 'Quoted'
    ACTIVE = 'Active'
    ACCEPTED = 'Accepted'
    CUSTOMER_POLICY_LIST = [QUOTED, ACTIVE, ACCEPTED]
    CUSTOMER_POLICY_CHOICES = (
        (QUOTED, 'Quoted'),
        (ACTIVE, 'Active'),
        (ACCEPTED, 'Accepted')
    )

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    policy = models.ForeignKey(Policy, on_delete=models.CASCADE)
    state = models.CharField(max_length=20, choices=CUSTOMER_POLICY_CHOICES)
    quoted_on = models.DateTimeField(auto_now_add=True)
    accepted_on = models.DateTimeField(blank=True, null=True)
    active_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = 'customer policy'
        verbose_name_plural = 'customer policies'

    def __str__(self):
        return "{} - {}".format(self.customer, self.state)

