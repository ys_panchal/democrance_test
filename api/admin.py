from django.contrib import admin
from api.models import (
    Customer,
    Policy,
    CustomerPolicy)

# Register Customer model for admin site
admin.site.register(Customer)
admin.site.register(Policy)
admin.site.register(CustomerPolicy)

