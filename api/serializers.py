import datetime
from django.utils import timezone
from rest_framework import serializers
from api.models import (
    Customer,
    CustomerPolicy,
    Policy)


class CustomerSerializer(serializers.ModelSerializer):
    """
    Customer Serializer
    """
    class Meta:
        model = Customer
        fields = ('first_name', 'last_name', 'dob')


class QuoteSerializer(serializers.ModelSerializer):
    """
    Create Quote Serializer
    """
    customer_id = serializers.IntegerField(write_only=True, required=False)
    type = serializers.CharField(source='policy.type', required=False)
    premium = serializers.IntegerField(read_only=True, source='policy.premium')
    cover = serializers.IntegerField(read_only=True, source='policy.cover')
    state = serializers.ChoiceField(choices=CustomerPolicy.CUSTOMER_POLICY_CHOICES, required=False)

    class Meta:
        model = CustomerPolicy
        fields = ('id', 'customer_id', 'type', 'premium', 'cover', 'state')

    def get_age(self, customer):
        """
        Returns the age of customer using date of birth
        """
        today = datetime.datetime.today().date()
        dob = customer.dob
        calculated_age = today.year - dob.year - (
            (today.month, today.day) < (dob.month, dob.day)
        )

        age_list = [10, 20, 30, 35, 40, 60, 90]
        for age in age_list:
            if age > calculated_age:
                calculated_age = age
                break

        return calculated_age

    def validate_customer_id(self, value):
        """
        Returns the validated customer id, if customer id is
        invalid then raises the serializer validation error
        """
        if not Customer.objects.filter(id=value).exists():
            raise serializers.ValidationError('Invalid customer id')
        return value

    def validate_type(self, value):
        """
        Returns the validated policy type, if policy type is
        invalid then raises the serializer validation error
        """
        if value not in Policy.POLICY_TYPE_LIST:
            raise serializers.ValidationError('Invalid policy type')
        return value

    def create(self, validated_data):
        """
        Override serializer create method to create the customer
        policy object
        """
        customer_id = validated_data.get('customer_id')
        type = validated_data.get('policy').get('type')
        customer = Customer.objects.get(id=customer_id)
        age = self.get_age(customer)
        policy = Policy.objects.get(type=type, age_limit=age)
        customer_policy = CustomerPolicy.objects.create(
            customer=customer, policy=policy, state=CustomerPolicy.QUOTED)
        return customer_policy

    def update(self, instance, validated_data):
        state = validated_data.get('state')
        instance.state = state
        if state == CustomerPolicy.ACCEPTED:
            instance.accepted_on = timezone.now()

        if state == CustomerPolicy.ACTIVE:
            instance.active_on = timezone.now()
        instance.save()
        return instance

