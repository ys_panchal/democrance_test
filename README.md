# Democrance Test

1) Create docker containers
>> docker-compose up -d

2) Migrate database
>> docker-compose exec web python manage.py migrate

3) Create User for accessing django admin
>> docker-compose exec web python manage.py createsuperuser

4) Load Policy fixtures
>> docker-compose exec web python manage.py loaddata policy.json

5) Login into django admin and validate policies are added
>> http://localhost:8000/admin

6) Create customer

Method: POST

Post Data: {
  "first_name": "Ben",
  "last_name": "Stokes",
  "dob": "1991-06-25"
}

URL : /api/v1/create_customer/

7) Create Quote

Method: POST

Post Data: {
    "customer_id": 2,
    "type": "health-insurance"
}

URL : /api/v1/quote/
 
 8) Accept Quote

 METHOD: POST

 Post Data: {
    "state": "Accepted"
}

 URL : /api/v1/quote/1

 9) Active Quote

 METHOD: POST

 Post Data: {
    "state": "Active"
}

 URL : /api/v1/quote/1

 10) List all Policies

 METHOD : GET

 URL : /api/v1/policies

 11) List policies for customer

 METHOD : GET

 URL : /api/v1/policies?customer_id=2
